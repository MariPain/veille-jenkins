const decora = require('./app');

test('decora function test', () => {
    expect(decora('1')).toBe('* 1');
    expect(decora('*')).toBe('* *');
    expect(decora('')).toBe('* ');
});
