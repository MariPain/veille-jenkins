# Veille-Jenkins

<details>
<summary>Français</summary>

J'ai suivi plusieurs étapes pour configurer Jenkins et le connecter à GitLab afin de réaliser une démonstration de fonctionnement basique.

La première chose que j'ai faite a été d'installer Jenkins sur une machine virtuelle Debian en utilisant VirtualBox. Ensuite, j'ai procédé à connecter Jenkins à mon dépôt GitLab.

Pour la démonstration, j'ai décidé d'effectuer un test simple d'intégration continue. J'ai créé un projet libre dans Jenkins qui surveille le dépôt GitLab. Fondamentalement, cette tâche simple consiste à me notifier s'il y a des modifications dans le code. Cependant, à partir de cette configuration de base, plusieurs tâches ou exécutions de code peuvent être programmées pour l'intégration, les tests, les performances, entre autres aspects.

Cette approche m'a permis de démontrer comment Jenkins peut interagir avec GitLab et automatiser efficacement des tâches liées au code.

*Pour voir un guide plus détaillé du processus, vous pouvez consulter le document  [step-by-step.md](step-by-step.md)*

Documentation de référence : 
- https://www.jenkins.io/doc/book/installing/linux/
- https://docs.gitlab.com/ee/integration/jenkins.html

</details>

<details>
<summary>English</summary>

I have followed several steps to configure Jenkins and connect it to GitLab in order to perform a basic functionality demonstration.

The first thing I did was to install Jenkins on a Debian virtual machine using VirtualBox. Then, I proceeded to connect Jenkins to my GitLab repository.

For the demonstration, I decided to perform a simple continuous integration test. I created a freestyle project in Jenkins that monitors the GitLab repository. Basically, this simple task consists of notifying me if there are modifications in the code. However, from this basic configuration, multiple tasks or code executions can be programmed for integration, testing, performance, among other aspects.

This approach allowed me to demonstrate how Jenkins can interact with GitLab and automate tasks related to the code effectively.

To see a more detailed guide of the process, you can go to the document [step-by-step.md](step-by-step.md)


Reference Documentation:
- https://www.jenkins.io/doc/book/installing/linux/
- https://docs.gitlab.com/ee/integration/jenkins.html

</details>

He seguido varios pasos para configurar Jenkins y conectarlo a GitLab con el fin de realizar una demostración de funcionamiento básico. 

Lo primero que hice fue instalar Jenkins en una máquina virtual Debian utilizando VirtualBox. Luego, procedí a conectar Jenkins a mi repositorio en GitLab.

Para la demostración, decidí realizar una prueba sencilla de integración continua. Creé un proyecto de estilo libre en Jenkins que monitorea el repositorio de GitLab. Básicamente, esta tarea sencilla consiste en avisarme si hay modificaciones en el código. Sin embargo, desde esta configuración básica, se pueden programar múltiples tareas o ejecuciones de código para integración, pruebas, rendimiento, entre otros aspectos. 

De esta forma puedo demostrar cómo Jenkins interactua con GitLab para automatizar tareas relacionadas con el código de manera efectiva.

Para ver una guía más detallada del proceso puedes ir al documento [step-by-step.md](step-by-step.md)


Documentacion de referencia : 
- https://www.jenkins.io/doc/book/installing/linux/
- https://docs.gitlab.com/ee/integration/jenkins.html

