<details>
<summary>Français</summary>
<h3>Étape 1 : Installer Jenkins dans Debian</h3>

1. Ouvrez le terminal sur votre machine virtuelle Debian.

2. Mettez à jour l'index des paquets :
   ```
   sudo apt update
   ```

3. Installez Java si ce n'est pas déjà fait :
   ```
   sudo apt install fontconfig openjdk-17-jre
   java -version
   ```

4. Installez git :
   ```
   sudo apt install git
   ```

5. Installez Jenkins :
   ```bash
   sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
     https://pkg.jenkins.io/debian/jenkins.io-2023.key
   echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
     https://pkg.jenkins.io/debian binary/ | sudo tee \
     /etc/apt/sources.list.d/jenkins.list > /dev/null
   sudo apt-get update
   sudo apt-get install jenkins
   ```

6. Démarrez le service Jenkins et activez le démarrage automatique :
   ```
   sudo systemctl start jenkins
   sudo systemctl enable jenkins
   ```

7. Vérifiez le statut du service pour vous assurer que Jenkins fonctionne correctement :
   ```
   sudo systemctl status jenkins
   ```

8. Notez l'adresse IP de votre machine Debian pour accéder à Jenkins via le navigateur web.

*Assurez-vous d'avoir correctement configuré les règles de redirection de port sur votre machine virtuelle VirtualBox.*

<h3> Étape 2 : Connexion de Jenkins à GitLab </h3>

1. Ouvrez votre navigateur web et accédez à l'adresse IP de votre machine Debian suivie de `:8080` (le port par défaut de Jenkins).

2. La première vue nous invite à déverrouiller Jenkins. Suivez les instructions à l'écran pour compléter la configuration initiale de Jenkins.

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
   Cela affichera le jeton requis, une fois entré, continuez la configuration initiale, y compris l'installation du plugin.

3. Une fois la configuration initiale terminée, Jenkins sera prêt à être utilisé. Connectez-vous à Jenkins avec les identifiants d'administrateur que vous avez configurés lors de l'installation.

**Connexion avec GitLab**

1. Tout d'abord, vous devez installer le plugin "GitLab Integration" dans Jenkins. Vous pouvez le faire depuis la section "Gérer Jenkins" -> "Gérer les plugins" -> "Disponibles". Recherchez "GitLab Integration" dans la liste des plugins disponibles, sélectionnez-le et cliquez sur "Installer sans redémarrage".

2. Une fois le plugin installé, rendez-vous sur votre compte GitLab pour obtenir un jeton d'accès. Accédez à votre profil utilisateur et sélectionnez "Paramètres". Dans la barre latérale gauche, cliquez sur "Tokens d'accès". Ensuite, cliquez sur "Créer un jeton d'accès personnel". Fournissez un nom descriptif pour le jeton et sélectionnez les autorisations nécessaires, telles que "api" et "read_repository". Ensuite, cliquez sur "Créer un jeton d'accès personnel". Copiez le jeton généré et enregistrez-le dans un endroit sûr.

3. Maintenant, retournez à la configuration système de Jenkins. Allez à "Gérer Jenkins" et sélectionnez "Configurer le système". Faites défiler jusqu'à la section "GitLab" et fournissez l'URL de votre instance GitLab, ainsi que le jeton d'accès que vous avez généré précédemment. Une fois que vous avez configuré la connexion, assurez-vous de sauvegarder les modifications. Après cela, vous pouvez tester la connexion pour vérifier que Jenkins peut communiquer correctement avec GitLab.

**Option pour configurer une clé SSH pour établir une connexion sécurisée entre Jenkins et GitLab.**

- Générez une nouvelle clé SSH sur votre machine où Jenkins est installé. Accédez au répertoire approprié avec l'utilisateur approprié :
```
cd /var/lib/jenkins
su -jenkins

```
Et une fois ici, nous générons une paire de clés avec `ssh -keygen`

Dans le répertoire `ssh`, nous avons deux fichiers `id_rsa` l'un avec la clé publique et l'autre avec la clé privée. Nous copions la clé publique dans GitLab. Pour ce faire, nous allons dans les clés SSH dans notre `Profil d'utilisateur` dans gitlab avec le titre `jenkins@jenkins`.

Dans Jenkins, nous ferons la même chose avec la clé privée. Nous allons à `Système` -> `credentials` et collons la clé privée qui se trouve dans le fichier de la machine jenkins.

**Étape 3 : Configuration de la tâche Jenkins**

**Tâche**

1. Depuis le tableau de bord de Jenkins, cliquez sur "Nouvel élément" dans le menu latéral.

2. Entrez un nom pour votre tâche et sélectionnez le type de projet que vous souhaitez créer, par exemple, "Projet libre".

3. Dans la section de configuration de la tâche, sous "Construction", cliquez sur "Ajouter une étape de construction" et choisissez "Exécuter une commande shell" (ou "Exécuter une commande shell" si vous êtes en espagnol).

4. Dans le champ de commande, écrivez une commande de test, par exemple :
   ```
   echo "La tâche a été exécutée avec succès!"
   ```

5. Dans la section "Gestion du code source", sélectionnez Git et fournissez l'URL de votre dépôt sur GitLab.

6. Dans la section "Déclencheurs de construction", cochez la case "Construire lorsqu'un changement est poussé vers GitLab".

7. Enregistrez la configuration de la tâche.

**Étape 4 : Démonstration**

1. Apportez une modification à votre code et téléchargez-le dans votre dépôt sur GitLab depuis l'interface web de GitLab.

2. Vérifiez que Jenkins détecte automatiquement les modifications dans votre dépôt et déclenche la tâche configurée. Vous pouvez vérifier cela en accédant à la page d'accueil de Jenkins et en observant si votre tâche apparaît dans la liste des tâches actives.

3. Accédez à la console de Jenkins pour voir la sortie de la commande echo que vous avez exécutée lors de l'étape précédente. Pour ce faire, cliquez sur le nom de votre tâche sur la page d'accueil de Jenkins, puis sélectionnez "Sortie de la console".

4. Vérifiez que le message "La tâche a été exécutée avec succès!" apparaît dans la sortie de la console. Cela confirmera que Jenkins a exécuté la tâche correctement.


### Introduction :

Nous allons configurer une tâche dans Jenkins pour construire et exécuter un projet Java stocké dans un dépôt GitLab. Cette tâche automatisera le processus de construction et de test du code, facilitant l'intégration continue dans le flux de travail de développement.

### Étapes :

1. **Création d'une nouvelle tâche dans Jenkins :**
   - Accédez à l'interface web de Jenkins et cliquez sur "Nouvel élément" dans le menu d'action.
   - Attribuez un nom à la tâche, par exemple, "Tâche d'intégration continue", et choisissez "Créer un projet libre".

2. **Configuration de l'origine du code source :**
   - Sélectionnez Git comme origine du code source.
   - Entrez l'URL du dépôt GitLab où se trouve le projet.

3. **Configuration de l'exécution :**
   - Ajoutez une nouvelle étape et sélectionnez "Exécuter une commande shell" ou son équivalent en fonction de votre système d'exploitation.
   - Dans la zone de commande, spécifiez les commandes nécessaires pour compiler et exécuter le projet Java. Assurez-vous d'ajuster les chemins et les noms de fichiers selon votre structure de projet.

4. **Exécution de la tâche :**
   - Enregistrez la configuration et cliquez sur "Construire maintenant" pour exécuter la tâche.
   - Vérifiez le résultat dans l'interface de Jenkins pour vous assurer que la tâche a été exécutée avec succès.


</details>
<details>
<summary>English</summary>

<h3>Step 1: Installation of Jenkins on Debian</h3>

1. Open the terminal on your Debian virtual machine.

2. Update the package index:
   ```
   sudo apt update
   ```

3. Install Java if not already installed:
   ```
   sudo apt install fontconfig openjdk-17-jre
   java -version
   ```

4. Install git:
   ```
   sudo apt install git
   ```

5. Install Jenkins:
   ```bash
   sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
     https://pkg.jenkins.io/debian/jenkins.io-2023.key
   echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
     https://pkg.jenkins.io/debian binary/ | sudo tee \
     /etc/apt/sources.list.d/jenkins.list > /dev/null
   sudo apt-get update
   sudo apt-get install jenkins
   ```

6. Start the Jenkins service and enable automatic startup:
   ```
   sudo systemctl start jenkins
   sudo systemctl enable jenkins
   ```

7. Verify the service status to ensure Jenkins is running correctly:
   ```
   sudo systemctl status jenkins
   ```

8. Note the IP address of your Debian machine to access Jenkins via the web browser.

*Make sure to have port forwarding rules properly configured on your VirtualBox virtual machine.*

### Step 2: Connecting Jenkins to GitLab

1. Open your web browser and navigate to the IP address of your Debian machine followed by `:8080` (Jenkins's default port).

2. The first view prompts us to unlock Jenkins. Follow the on-screen instructions to complete the initial Jenkins setup.

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
   This will display the required token, once entered continue the initial setup, including plugin installation.

3. Once the initial setup is complete, Jenkins will be ready to use. Log in to Jenkins with the administrator credentials you configured during installation.

**Connecting with GitLab**

1. First, you need to install the "GitLab Integration" plugin in Jenkins. You can do this from the "Manage Jenkins" -> "Manage Plugins" -> "Available" section. Search for "GitLab Integration" in the list of available plugins, select it, and click "Install without restart".

2. Once the plugin is installed, go to your GitLab account to obtain an access token. Go to your user profile and select "Settings". In the left sidebar, click on "Access Tokens". Then click on "Create a personal access token". Provide a descriptive name for the token and select the necessary permissions, such as "api" and "read_repository". Then click "Create personal access token". Copy the generated token and save it in a secure location.

3. Now, go back to Jenkins system configuration. Go to "Manage Jenkins" and select "Configure System". Scroll down to the "GitLab" section and provide the URL of your GitLab instance, as well as the access token you generated earlier. Once you have configured the connection, make sure to save the changes. After this, you can test the connection to verify that Jenkins can communicate properly with GitLab.

**Option to set up an SSH key for establishing a secure connection between Jenkins and GitLab.**

- Generate a new SSH key on your machine where Jenkins is installed. Navigate to the appropriate directory with the appropriate user:
```
cd /var/lib/jenkins
su -jenkins

```
And once here we generate a pair of keys with `ssh -keygen`

In the `ssh` directory we have two files `id_rsa` one with the public key and the other with the private key. We copy the public key into GitLab. To do this, we go to SSH keys in our `User Profile` in gitlab with the title `jenkins@jenkins`.

In Jenkins we will do the same with the private key. We go to `System` -> `credentials` and paste the private key that is in the jenkins machine file.

**Step 3: Configuring the Jenkins Job**

**Task**

1. From the Jenkins dashboard, click on "New Item" in the side menu.

2. Enter a name for your job and select the type of project you want to create, for example, "Freestyle project".

3. In the job configuration section, under "Build", click on "Add build step" and choose "Execute shell" (or "Run shell command" if you are in Spanish).

4. In the command field, write a test command, for example:
   ```
   echo "The task has been executed successfully!"
   ```

5. In the "Source Code Management" section, select Git and provide the URL of your repository on GitLab.

6. In the "Build Triggers" section, check the "Build when a change is pushed to GitLab" box.

7. Save the job configuration.

**Step 4: Demonstration**

1. Make a modification to your code and upload it to your repository on GitLab from the GitLab web interface.

2. Verify that Jenkins automatically detects the changes in your repository and triggers the configured job. You can check this by navigating to the Jenkins homepage and observing if your job appears in the list of active jobs.

3. Access the Jenkins console to see the output of the echo command you executed in the previous step. To do this, click on the name of your job on the Jenkins homepage, then select "Console Output".

4. Verify that the message "The task has been executed successfully!" appears in the console output. This will confirm that Jenkins has executed the job correctly.


### Introduction:

We are going to configure a job in Jenkins to build and execute a Java project stored in a GitLab repository. This job will automate the process of building and testing the code, making continuous integration in the development workflow easier.

***Step by step:***

1. **Creating a new job in Jenkins:**
   - Access the Jenkins web interface and click on "New Item" in the action menu.
   - Assign a name to the job, for example, "Continuous Integration Job", and choose "Create a freestyle project".

2. **Configuring the source code origin:**
   - Select Git as the source code origin.
   - Enter the URL of the GitLab repository where the project is located.

3. **Configuring the execution:**
   - Add a new step and select "Execute shell" or its equivalent based on your operating system.
   - In the command box, specify the necessary commands to compile and execute the Java project. Make sure to adjust the paths and

 file names according to your project structure.

4. **Executing the job:**
   - Save the configuration and click on "Build now" to execute the job.
   - Check the result in the Jenkins interface to ensure that the job has been completed successfully.



</details>

### Paso 1: Instalación de Jenkins en Debian

1. Abre la terminal en tu máquina virtual Debian.

2. Actualiza el índice de paquetes:
   ```
   sudo apt update
   ```

3. Instala Java si aún no está instalado:
   ```
   sudo apt install fontconfig openjdk-17-jre
   java -version
   ```

4. Instala git:
   ```
   sudo apt install git
   ```

5. Instala Jenkins:
```bash
sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
  https://pkg.jenkins.io/debian/jenkins.io-2023.key
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins
```

6. Inicia el servicio de Jenkins e habilita su inicio automático en el arranque:
   ```
   sudo systemctl start jenkins
   sudo systemctl enable jenkins
   ```

7. Verifica el estado del servicio para asegurarte de que Jenkins se esté ejecutando correctamente:
   ```
   sudo systemctl status jenkins
   ```

8. Anota la dirección IP de tu máquina Debian para acceder a Jenkins a través del navegador web.


*Importante tener bien configuradas las reglas de redireccion de puestos en nuestra Maquina virtual en virtual box*

### Paso 2: Conexión de Jenkins a GitLab

1. Abre tu navegador web y navega a la dirección IP de tu máquina Debian seguida de `:8080` (el puerto predeterminado de Jenkins).

2. La primera vista nos invita a desbloquear Jenkins.Sigue las instrucciones en pantalla para completar la configuración inicial de Jenkins.

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
   Esto nos mostrará el token requerido, una vez ingresado continua la configuracion inicial, incluida la la instalación de plugins.

3. Una vez completada la configuración inicial, Jenkins estará listo para ser utilizado. Inicia sesión en Jenkins con las credenciales de administrador que configuraste durante la instalación.

**Conexion con GitLab**

1. Primero, necesitas instalar el plugin "GitLab Integration" en Jenkins. Esto lo puedes hacer desde la sección "Administrar Jenkins" -> "Administrar complementos" -> "Disponibles". Busca "GitLab Integration" en la lista de plugins disponibles, selecciónalo y haz clic en "Instalar sin reiniciar".

2. Una vez que hayas instalado el plugin, dirígete a tu cuenta de GitLab para obtener un token de acceso. Ve a tu perfil de usuario y selecciona "Settings" (Configuración). En la barra lateral izquierda, haz clic en "Access Tokens" (Tokens de acceso). Luego, haz clic en "Create a personal access token" (Crear un token de acceso personal). Proporciona un nombre descriptivo para el token y selecciona los permisos necesarios, como "api" y "read_repository". Luego, haz clic en "Create personal access token" (Crear token de acceso personal). Copia el token generado y guárdalo en un lugar seguro.

3. Ahora, regresa a la configuración del sistema de Jenkins. Ve a "Administrar Jenkins" y selecciona "Configurar el sistema". Desplázate hasta la sección "GitLab" y proporciona la URL de tu instancia de GitLab, así como el token de acceso que generaste anteriormente. Una vez que hayas configurado la conexión, asegúrate de guardar los cambios. Después de esto, puedes probar la conexión para verificar que Jenkins pueda comunicarse correctamente con GitLab.

**Opcion de configurar una clave SSH para establecer una conexión segura entre Jenkins y GitLab.** 

- Genera una nueva clave SSH en tu máquina donde está instalado Jenkins. Nos ubicamos en el directorio adecuado con el usuario adecuado:
```
cd /var/lib/jenkins
su -jenkins

```
Y una vez aqui generamos un par de claves con `ssh -keygen`

En el directorio `ssh` tenemos dos ficheros `id_rsa` uno con la clave publica y el otro con la clave privada. Copiamos la clave publica en GitLab.  Para eso nos dirigimos a claves SSH en nuestro `User Profile` de gitlab con el titulo `jenkins@jenkins`.

En jenkins haremos lo mismo con la clave pribada. Nos dirigimos a `System` -> `credentials` y pegamos la clave privada que esta en el fichero de la maquina jenkins.

**Paso 3: Configuración del trabajo en Jenkins**

**Tarea**

1. Desde el panel de Jenkins, haz clic en "New Item" en el menú lateral.

2. Introduce un nombre para tu trabajo y selecciona el tipo de proyecto que deseas crear, por ejemplo, "Freestyle project".

3. En la sección de configuración del trabajo, debajo de "Build", haz clic en "Add build step" y elige "Execute shell" (o "Ejecutar comando shell" si estás en español).

4. En el campo de comandos, escribe un comando de prueba, por ejemplo:
   ```
   echo "¡La tarea se ha ejecutado correctamente!"
   ```

5. En la sección "Source Code Management", selecciona Git y proporciona la URL de tu repositorio en GitLab.

6. En la sección "Build Triggers", marca la casilla "Build when a change is pushed to GitLab".

7. Guarda la configuración del trabajo.


**Paso 4: Demostración**

1. Realiza una modificación en tu código y súbelo a tu repositorio en GitLab desde la interfaz web de GitLab.

2. Verifica que Jenkins detecte automáticamente los cambios en tu repositorio y active el trabajo configurado. Puedes comprobar esto navegando a la página principal de Jenkins y observando si tu trabajo aparece en la lista de trabajos activos.

3. Accede a la consola de Jenkins para ver la salida del comando echo que ejecutaste en el paso anterior. Para hacer esto, haz clic en el nombre de tu trabajo en la página principal de Jenkins, luego selecciona "Console Output" (o "Salida de la consola" si estás en español).

4. Verifica que el mensaje "¡La tarea se ha ejecutado correctamente!" aparezca en la salida de la consola. Esto confirmará que Jenkins ha ejecutado la tarea correctamente.



### Introducción:

Vamos a configurar una tarea en Jenkins para compilar y ejecutar un proyecto Java almacenado en un repositorio GitLab. Esta tarea se encargará de automatizar el proceso de construcción y prueba del código, lo que facilitará la integración continua en el flujo de trabajo de desarrollo.

### Paso a paso:

1. **Creación de una nueva tarea en Jenkins:**
   - Accede a la interfaz web de Jenkins y haz clic en "Nueva Tarea" en el menú de acciones.
   - Asigna un nombre a la tarea, por ejemplo, "Tarea de Integración Continua", y elige "Crear un proyecto de estilo libre".

2. **Configuración del origen del código fuente:**
   - Selecciona Git como el origen del código fuente.
   - Ingresa la URL del repositorio GitLab donde se encuentra el proyecto.

3. **Configuración de la ejecución:**
   - Agrega un nuevo paso y selecciona "Ejecutar línea de comandos (shell)" o su equivalente según el sistema operativo.
   - En el cuadro de comandos, especifica los comandos necesarios para compilar y ejecutar el proyecto Java. Asegúrate de ajustar las rutas y los nombres de archivos según la estructura de tu proyecto.

4. **Ejecución de la tarea:**
   - Guarda la configuración y haz clic en "Construir ahora" para ejecutar la tarea.
   - Verifica el resultado en la interfaz de Jenkins para asegurarte de que la tarea se haya completado con éxito.


